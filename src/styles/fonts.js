class Fonts {
  static FONT_ROBOTO_REG = "Roboto-Regular";
  static FONT_ROBOTO_BOLD = "Roboto-Bold";
  static FONT_ROBOTO_BACK = "Roboto-Black";
  static FONT_ROBOTO_LIGHT = "Roboto-Light";
  static FONT_ROBOTO_LIGHTITALIC = "Roboto-LightItalic";
}
export default Fonts;
