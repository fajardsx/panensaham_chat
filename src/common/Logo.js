/* @flow strict-local */
import React from 'react';
import type { Node } from 'react';
import { Image } from 'react-native';

import logoImg from '../../static/img/logo.png';
import logoKomunistasPS from '../../static/img/logo-komunitas-ps.png';
import { createStyleSheet } from '../styles';

const styles = createStyleSheet({
  logo: {
    width: 40,
    height: 40,
    margin: 20,
    alignSelf: 'center',
  },
  logoKomunias: {
    width: '80%',
    height: 100,
    margin: 20,
    alignSelf: 'center',
  },
  logoKomuniasFooter: {
    width: '40%',
    height: 40,
    margin: 0,
    alignSelf: 'center',
  },
});

export default (): Node => <Image style={styles.logo} source={logoImg} resizeMode="contain" />;

export const LogoKomunitas = () => (
  <Image style={styles.logoKomunias} source={logoKomunistasPS} resizeMode="contain" />
);

export const LogoKomunitasFooter = () => (
  <Image style={styles.logoKomuniasFooter} source={logoKomunistasPS} resizeMode="contain" />
);
