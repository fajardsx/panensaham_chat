/* @flow strict-local */
import React, { PureComponent } from 'react';
import type { ComponentType } from 'react';
import { View, Linking, Image, Dimensions, Text, ImageBackground } from 'react-native';

import type { RouteProp } from '../react-navigation';
import type { AppNavigationProp } from '../nav/AppNavigator';
import type { Dispatch } from '../types';
import { createStyleSheet } from '../styles';
import { connect } from '../react-redux';
import * as api from '../api';
import {
  ErrorMsg,
  Input,
  PasswordInput,
  Screen,
  WebLink,
  ZulipButton,
  ViewPlaceholder,
  RawLabel,
} from '../common';
import { isValidEmailFormat } from '../utils/misc';
import { loginSuccess } from '../actions';
import { LogoKomunitasFooter } from '../common/Logo';
import Fonts from '../styles/fonts';

const styles = createStyleSheet({
  linksTouchable: {
    marginTop: 10,
    alignItems: 'flex-end',
  },
  forgotPasswordText: {
    textAlign: 'right',
    fontFamily: Fonts.FONT_ROBOTO_REG,
  },
  buttonLogin: {
    width: 200,
    backgroundColor: '#442b1a',
    paddingVertical: 5,
    fontFamily: Fonts.FONT_ROBOTO_REG,
    elevation: 1,
    marginHorizontal: 70,
  },
  buttonForgotPassword: {
    width: 350,
    backgroundColor: 'transparent',
    paddingVertical: 5,
    fontFamily: Fonts.FONT_ROBOTO_REG,
    marginHorizontal: 70,
  },
  buttonSignup: {
    width: 200,
    marginTop: 20,
    backgroundColor: '#d98428',
    marginHorizontal: 70,
    fontFamily: Fonts.FONT_ROBOTO_REG,
    elevation: 1,
    height: 30,
  },
  buttonTextLogin: {
    color: '#ffffff',
    fontSize: 21,
    fontFamily: Fonts.FONT_ROBOTO_REG,
  },
  buttonForgotTextLogin: {
    width: '100%',
    color: '#000000',
    fontSize: 16,
    textAlign: 'right',
    fontFamily: Fonts.FONT_ROBOTO_REG,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 12,
    fontFamily: Fonts.FONT_ROBOTO_REG,
  },
  inpurConainer: {
    width: 350,
    height: 40,
    backgroundColor: '#fff',
    alignSelf: 'center',
    borderRadius: 50,
    fontFamily: Fonts.FONT_ROBOTO_REG,
    paddingHorizontal: 20,
  },
  inputDesc: {
    width: 350,
    alignSelf: 'center',
    fontFamily: Fonts.FONT_ROBOTO_REG,
    paddingHorizontal: 20,
  },
  bgSize: {
    position: 'absolute',
  },
});

type OuterProps = $ReadOnly<{|
  // These should be passed from React Navigation
  navigation: AppNavigationProp<'password-auth'>,
  route: RouteProp<'password-auth', {| realm: URL, requireEmailFormat: boolean |}>,
|}>;

type SelectorProps = $ReadOnly<{||}>;

type Props = $ReadOnly<{|
  ...OuterProps,

  dispatch: Dispatch,
  ...SelectorProps,
|}>;

type State = {|
  email: string,
  password: string,
  error: string,
  progress: boolean,
|};

class PasswordAuthScreenInner extends PureComponent<Props, State> {
  state = {
    progress: false,
    email: '',
    password: '',
    error: '',
  };

  tryPasswordLogin = async () => {
    const { dispatch, route } = this.props;
    const { requireEmailFormat, realm } = route.params;
    const { email, password } = this.state;

    this.setState({ progress: true, error: undefined });

    try {
      const fetchedKey = await api.fetchApiKey({ realm, apiKey: '', email }, email, password);
      this.setState({ progress: false });
      dispatch(loginSuccess(realm, fetchedKey.email, fetchedKey.api_key));
    } catch (err) {
      this.setState({
        progress: false,
        error: requireEmailFormat
          ? 'Wrong email or password. Try again.'
          : 'Wrong username or password. Try again.',
      });
    }
  };

  validateForm = () => {
    const { requireEmailFormat } = this.props.route.params;
    const { email, password } = this.state;

    if (requireEmailFormat && !isValidEmailFormat(email)) {
      this.setState({ error: 'Enter a valid email address' });
    } else if (!requireEmailFormat && email.length === 0) {
      this.setState({ error: 'Enter a username' });
    } else if (!password) {
      this.setState({ error: 'Enter a password' });
    } else {
      this.tryPasswordLogin();
    }
  };

  signupclick = () => {
    let link = 'https://www.panensaham.com/mychat-signup.php';
    Linking.canOpenURL(link)
      .then(supported => {
        if (!supported) {
          console.log('not support');
        } else {
          return Linking.openURL(link);
        }
      })
      .catch(err => console.error('An error occurred', err));
  };
  forgotPasswordclick = () => {
    let link = 'https://mychat.panensaham.com/accounts/password/reset/';
    Linking.canOpenURL(link)
      .then(supported => {
        if (!supported) {
          console.log('not support');
        } else {
          return Linking.openURL(link);
        }
      })
      .catch(err => console.error('An error occurred', err));
  };

  render() {
    const { requireEmailFormat, realm } = this.props.route.params;
    const { email, password, progress, error } = this.state;
    const isButtonDisabled =
      password.length === 0 ||
      email.length === 0 ||
      (requireEmailFormat && !isValidEmailFormat(email));

    return (
      <Screen
        title="Log in"
        disableheader={true}
        style={{ alignItems: 'center', flex: 1 }}
        keyboardShouldPersistTaps="always"
        shouldShowLoadingBanner={false}
      >
        <ImageBackground
          style={{ flex: 1, width: Dimensions.get('screen').width, alignItems: 'center' }}
          resizeMode={'cover'}
          source={require('../../static/img/bglogin.jpg')}
        >
          <Text
            style={{
              color: '#442b1a',
              fontSize: 21,
              marginVertical: 20,
              fontFamily: Fonts.FONT_ROBOTO_BACK,
            }}
          >
            {'Selamat bergabung di'}
          </Text>
          <View style={{ flex: 0.65 }} />
          <View style={[styles.inputDesc, { marginVertical: 20 }]}>
            <Text
              style={{
                fontFamily: Fonts.FONT_ROBOTO_BACK,
                color: '#442b1a',
                fontSize: 13,
                textAlign: 'center',
                width: 320,
              }}
            >
              {'ps:chat '}
              <Text
                style={{
                  fontFamily: Fonts.FONT_ROBOTO_REG,
                  color: '#442b1a',
                  fontSize: 12,
                  textAlign: 'center',
                }}
              >
                {
                  'adalah platform char komunitas yang luar biasa\ndimana para investor, trader, atau bahkan masyarakat\numum bisa berdiskusi tentang semua hal mulai dari Saham,\nCyrpto, Forex, Komoditas dan peluang lainnya.\nYuk bergabung sekarang!\n'
                }
              </Text>
              {'PS: Di sini, #kamugasendirian loh.'}
            </Text>
          </View>

          <View style={styles.inpurConainer}>
            <Input
              autoFocus={email.length === 0}
              autoCapitalize="none"
              autoCorrect={false}
              blurOnSubmit={false}
              keyboardType={requireEmailFormat ? 'email-address' : 'default'}
              placeholder={requireEmailFormat ? 'Username / Email' : 'Username'}
              defaultValue={email}
              onChangeText={newEmail => this.setState({ email: newEmail })}
            />
          </View>
          <ViewPlaceholder height={8} />
          <View style={styles.inpurConainer}>
            <PasswordInput
              autoFocus={email.length !== 0}
              placeholder="Password"
              value={password}
              onChangeText={newPassword => this.setState({ password: newPassword })}
              blurOnSubmit={false}
              onSubmitEditing={this.validateForm}
            />
          </View>
          <ZulipButton
            style={styles.buttonForgotPassword}
            textStyle={styles.buttonForgotTextLogin}
            disabled={false}
            text="Forgot password?"
            progress={progress}
            onPress={this.forgotPasswordclick}
          />
          <ViewPlaceholder height={5} />
          <ZulipButton
            style={styles.buttonLogin}
            textStyle={styles.buttonTextLogin}
            disabled={isButtonDisabled}
            text="Log in"
            progress={progress}
            onPress={this.validateForm}
          />
          <ZulipButton
            style={styles.buttonSignup}
            textStyle={styles.buttonText}
            disabled={false}
            text="Or Sign up here"
            progress={progress}
            onPress={this.signupclick}
          />
          <View
            style={{
              width: Dimensions.get('screen').width,
              backgroundColor: '#fff',
              paddingVertical: 10,
              position: 'absolute',
              top: Dimensions.get('screen').height * 0.835,
            }}
          >
            <LogoKomunitasFooter />
            <Text
              style={{ fontWeight: '900', color: '#442b1a', fontSize: 10, textAlign: 'center' }}
            >
              {'Copyright Komunitas Panen Saham 2021. All right reserved.'}
            </Text>
          </View>
          <ErrorMsg error={error} />
        </ImageBackground>

        {/* <View style={styles.linksTouchable}>
          <RawLabel style={styles.forgotPasswordText}>
            <WebLink label="Forgot password?" url={new URL('/accounts/password/reset/', realm)} />
          </RawLabel>
        </View> */}
      </Screen>
    );
  }
}

const PasswordAuthScreen: ComponentType<OuterProps> = connect<{||}, _, _>()(
  PasswordAuthScreenInner,
);

export default PasswordAuthScreen;
